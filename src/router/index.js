import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

//常量路由： 无论什么用户登录
export const constantRoutes = [
  {
    path: '/',
    redirect: '/login'
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('../views/LoginRegister.vue')
  },
  {
    path: '/main',
    name: 'main',
    component: () => import('../views/Main.vue'),
    redirect: '/home',
    children:[]
  }
]

//异步路由： 不同用户登录所展示的路由不同
export const asyncRoutes = [
  {
    path: '/home',
    name: 'home',
    component: () => import("../views/Home.vue")
  },
  {
    path: '/punch',
    name: 'punch',
    component: () => import('../views/Punch.vue')
  },
  {
    path: '/punchrecord',
    name: 'punchrecord',
    component: () => import('../views/PunchRecord.vue')
  },
  {
    path: '/person',
    name: 'person',
    component: () => import('../views/PersonInfo.vue')
  },
  {
    path: '/modify',
    name: 'modify',
    component: () => import('../views/ModifyInfo.vue')
  },
  {
    path: '/account',
    name: 'account',
    component: () => import('../views/Account.vue')
  },
  {
    path: '/statistic',
    name: 'statistic',
    component: () => import('../views/Statistic.vue')
  },
]

//任意路由：当路径出现错误的时候重定向404
export const anyRoutes = [
  {
    path: "/404",
    name: "notFound",
    component: () => import('../views/NotFound.vue')
  },
  {
    path: "*",
    redirect: "/404"
  }
]


const createRouter = () => new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: constantRoutes
})

const router = createRouter()

export default router

const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push(location, onResolve, onReject) {
  if (onResolve || onReject) return originalPush.call(this, location, onResolve, onReject)
  return originalPush.call(this, location).catch(err => err)
}

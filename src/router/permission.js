import router from "./index"
import store from "@/store"

setTimeout(
  router.beforeEach((to, form, next) => {
    const token = store.state.user.token
    if (token) {
      if (to.name === 'login') {
        console.log("111")
        next({ name: 'main' })
      } else {
        if (!store.state.user.userInfo.username) {
          store.dispatch("getUserInfo")
          console.log("222")
          next()
        } else {
          try {
            next()
          } catch (error) {
            console.log("444")
            store.dispatch("logout")
            next({ name: 'login' })
          }
        }
      }
    } else{
      if (to.name === 'login') {
        console.log("555")
        next()
      } else {
        console.log("666")
        next({ name: 'login' })
      }
    }
  }), 100)



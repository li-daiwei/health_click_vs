import requests from "./requests";

export const punch = (data) => requests({
  url: '/punch',
  method: 'post',
  data
})

export const punchRecord = (data) => requests({
  url: '/record',
  method: 'post',
  data
})


export const pageTotal = (data) => requests({
  url: '/total',
  method: 'get',
  params: { username: data }
})

export const recentRecord = () => requests({
  url: '/recent',
  method: 'get'
})

export const setPunchName = () => requests({
  url: '/getName',
  method: 'get'
})

export const userInfo = () => requests({
  url: '/userInfo',
  method: 'get'
})

export const addUser = (data) => requests({
  url: '/addUser',
  method: 'post',
  data
})

export const deleteAccout = (data) => requests({
  url: '/delete',
  method: 'get',
  params: { id: data }
})

export const modifyUser = (data) => requests({
  url: '/modifyUser',
  method: 'post',
  data
})

export const statisticBar = () => requests({
  url: '/statistic/bar',
  method: 'get'
})

export const statisticPie = () => requests({
  url:'/statistic/pie',
  method: 'get'
})

export const statisticLine = () =>  requests({
  url: '/statistic/line',
  method: 'get'
})

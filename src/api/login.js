import requests from "./requests"

export const login = (username, password) => {
  const data = {
    username,
    password
  }
  return requests({
    url: '/login',
    method: 'post',
    data
  })
}

export const logout = () => requests({
    url: '/logout',
    method: 'get',
  })

export const register = (username, password) => {
  const data = {
    username,
    password
  }
  return requests({
    url: '/register',
    method: 'post',
    data
  })
}

export const getUserInfo = () => requests({
  url: '/info',
  method: 'get'
})

export const modifyInfo = (form) => {
  const data = {
    newpassword: form.newpassword
  }
  return requests({
    url: '/modify',
    method: 'post',
    data
  })
}

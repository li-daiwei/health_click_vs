import Vue from 'vue'
import Vuex from 'vuex'
import user from  './user'
import home from  './home'
import tab from './tab'
Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    user,
    home,
    tab
  }
})

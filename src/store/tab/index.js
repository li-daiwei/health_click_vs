export default {
  state: {
    tabsList: [
      {
        path: '/main',
        name: 'main',
        label: '首页',
        icon: 'el-icon-arrow-right'
      }],
  },
  mutations: {
    ADDTABLIST(state, tab) {
      if(state.tabsList.indexOf(tab) === -1) {
        state.tabsList.push(tab)
      }
    }
  },
  actions: {

  }
}

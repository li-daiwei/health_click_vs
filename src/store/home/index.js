import { punch, punchRecord, pageTotal, recentRecord, setPunchName, userInfo, addUser, deleteAccout, modifyUser } from '@/api/punch'
import { Message } from 'element-ui'
const state = {
  isCollapse: true,
  punchRecord: [],
  pageTotal: 0,
  recentRecord: '',
  tag_color: '',
  showRouter: true,
  punchName: [],
  userList: []
}

const mutations = {
  changeIsCollapse(state) {
    state.isCollapse = !state.isCollapse
  },
  punchRecord(state, data) {
    state.punchRecord = data
  },
  pageTotal(state, data) {
    state.pageTotal = data
  },
  recentRecord(state, data) {
    if (data.status === "健康") {
      state.tag_color = "tag_green"
    }
    else {
      state.tag_color = "tag_red"
    }
    state.recentRecord = data;
  },
  setPunchName(state, data) {
    state.punchName = data
  },
  setAccount(state, data) {
    state.userList = data
  }
}

const actions = {
  punch({ commit }, data) {
    return new Promise((resolve, reject) => {
      punch(data).then(data => {
        if (data.success) {
          Message({
            type: 'success',
            showClose: true,
            message: "太棒了，打卡成功了呦！"
          })
          resolve()
        } else {
          Message({
            type: 'error',
            showClose: true,
            message: "亲！打卡失败了呦！"
          })
          reject()
        }
      }).catch(error => {
        reject()
      })
    })
  },
  punchRecord({ commit }, data) {
    return new Promise((resolve, reject) => {
      punchRecord(data).then(data => {
        if (data.success) {
          commit('punchRecord', data.data)
          resolve()
        } else {
          reject()
        }
      }).catch(error => {
        reject()
      })
    })
  },
  pageTotal({ commit }, data) {
    return new Promise((resolve, reject) => {
      pageTotal(data).then(data => {
        if (data.success) {
          commit('pageTotal', data.data)
          resolve()
        } else {
          reject()
        }
      }).catch(error => {
        reject()
      })
    })
  },
  recentRecord({ commit }) {
    return new Promise((resolve, reject) => {
      recentRecord().then(data => {
        if (data.success) {
          commit('recentRecord', data.data)
          resolve()
        } else {
          reject()
        }
      }).catch(error => {
        reject()
      })
    })
  },
  setPunchName({ commit }) {
    return new Promise((resolve, reject) => {
      setPunchName().then(data => {
        if (data.success) {
          commit('setPunchName', data.data)
          resolve()
        } else {
          reject()
        }
      }).catch(error => {
        reject()
      })
    })
  },
  userInfo({ commit }) {
    return new Promise((resolve, reject) => {
      userInfo().then(data => {
        if (data.success) {
          commit('setAccount', data.data)
          resolve()
        } else {
          reject()
        }
      }).catch(error => {
        reject()
      })
    })
  },
  addUser({ commit }, form) {
    return new Promise((resolve, reject) => {
      addUser(form).then(data => {
        if (data.success) {
          Message({
            type: "success",
            message: "添加成功"
          })
          resolve()
        } else {
          reject()
        }
      }).catch(error => {
        reject()
      })
    })
  },
  deleteAccout({ commit }, data) {
    return new Promise((resolve, reject) => {
      deleteAccout(data.id).then(data => {
        if (data.success) {
          Message({
            type: "success",
            message: "删除成功"
          })
          resolve()
        } else {
          reject()
        }
      }).catch(error => {
        reject()
      })
    })
  },
  modifyUser({ commit }, data) {
    return new Promise((resolve, reject) => {
      modifyUser(data).then(data => {
        if (data.success) {
          Message({
            type: "success",
            message: "修改成功"
          })
          resolve()
        }else {
          reject()
        }
      }).catch(error => {
        reject()
      })
    })
  }
}

const getters = {}

export default {
  state,
  mutations,
  actions,
  getters
}
